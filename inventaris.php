<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Inventaris</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    

    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.html"><span class="navbar-brand"><span class="fa fa-paper-plane"></span> Inventaris</span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> Ratu Annisa
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a href="./">My Account</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Admin Panel</li>
                <li><a href="./">Users</a></li>
                <li><a href="./">Security</a></li>
                <li><a tabindex="-1" href="./">Payments</a></li>
                <li class="divider"></li>
                <li><a tabindex="-1" href="Login.php">Login</a></li>
                <li><a tabindex="-1" href="Logout.php">Logout</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
    

    <div class="sidebar-nav">
    <ul>
	  <!-- batas -->
	
   <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Dashboard<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">
            
   <li ><a href="index.php"><span class="fa fa-caret-right"></span> index</a></li>
    </ul></li>
  
  <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Inventaris<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">
            
       <li ><a href="data_inventaris.php"><span class="fa fa-caret-right"></span> Data Inventaris</a></li>
       <li ><a href="data_level.php"><span class="fa fa-caret-right"></span> Data Level</a></li>
      <li ><a href="inventaris.php"><span class="fa fa-caret-right"></span> Data barang masuk</a></li>
      <li ><a href="users.html"><span class="fa fa-caret-right"></span> Data barang keluar</a></li>
        


       <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Transaksi<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">


      <li ><a href="peminjaman.php"><span class="fa fa-caret-right"></span> peminjaman</a></li>
      <li ><a href="inventaris.php"><span class="fa fa-caret-right"></span> pengembalian</a></li>
       


         <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Users<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">
    
       <li ><a href="petugas.php"><span class="fa fa-caret-right"></span> Data petugas</a></li>
     <li ><a href="pegawai.php"><span class="fa fa-caret-right"></span> Data pegawai</a></li>
          
          <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Laporan<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">

      <li ><a href="ctk_inventaris1.php"><span class="fa fa-caret-right"></span>Data Laporan Absen</a></li>


       <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Pengaturan<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">


    <li ><a href="backup.php"><span class="fa fa-caret-right"></span> backup</a></li>


    </ul></li>

  
       

       
           
            </ul>
    </div>

    <div class="content">
        <div class="header">
            
            <h1 class="page-title">Data Barang Masuk</h1>
                    <ul class="breadcrumb">
            <li><a href="index.html">Home</a> </li>
            <li class="active">Users</li>
        </ul>

        </div>
        <div class="main-content">
            
<div class="btn-toolbar list-toolbar">
    <a href="tambah_b.php" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah </a>
    <a href="export_inventaris.php" class="btn btn-danger"><i></i> Export</a>
  <div class="btn-group">
  </div>
</div>
<table class="table">
  <thead>
    <tr>
     <th>No</th>
						<th>ID Inventaris</th>
						<th>Nama Inventaris</th>
						<th>Kondisi</th>
            <th>Spesifikasi</th>
						<th>Keterangan</th>
						<th>Jumlah</th>
						<th>Nama Jenis</th>
						<th>Tanggal Register</th>
						<th>Nama Ruang</th>
						<th>Kode Inventaris</th>
						<th>Nama Petugas</th>
            <th>Sumber</th>
            <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
            include 'koneksi.php';
            $no=1;
            $select = mysqli_query($koneksi,"SELECT * FROM inventaris INNER JOIN jenis ON  inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas order by id_inventaris desc ");
            while ($data = mysqli_fetch_array($select))
            {
                ?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $data['id_inventaris']; ?></td>
				<td><?php echo $data['nama']; ?></td>
				<td><?php echo $data['kondisi']; ?></td>
        <td><?php echo $data['spesifikasi']; ?></td>
				<td><?php echo $data['keterangan']; ?></td>	
				<td><?php echo $data['jumlah']; ?></td>
				<td><?php echo $data['nama_jenis']; ?></td>
				<td><?php echo $data['tanggal_register']; ?></td>
				<td><?php echo $data['nama_ruang']; ?></td>
				<td><?php echo $data['kode_inventaris']; ?></td>
				<td><?php echo $data['nama_petugas']; ?></td>
        <td><?php echo $data['sumber']; ?></td>
      <td>
           <a href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris'];?>"><i class="fa fa-pencil"></i></a>
          <a href="hapus_barang_masuk.php?id_inventaris=<?php echo $data['id_inventaris']; ?>" role="button" data-toggle="modal"><i class="fa fa-trash-o"></i></a>
 
    </tr>
	<?php
			}
			?>
   
  </tbody>
</table>

<ul class="pagination">
  <li><a href="#">&laquo;</a></li>
  <li><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li><a href="#">&raquo;</a></li>
</ul>

<div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Delete Confirmation</h3>
        </div>
        <div class="modal-body">
            <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?<br>This cannot be undone.</p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button class="btn btn-danger" data-dismiss="modal"></button>
        </div>
      </div>
    </div>
</div>


            <footer>
                <hr>

                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                
                @ Ujikom 2019 
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
