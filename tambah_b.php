<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Inventaris</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    

    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->
</head>
<body class=" theme-blue">
    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.html"><span class="navbar-brand"><span class="fa fa-paper-plane"></span> Inventaris</span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> Ratu Annisa
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a href="./">My Account</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Admin Panel</li>
                <li><a href="./">Users</a></li>
                <li><a href="./">Security</a></li>
                <li><a tabindex="-1" href="./">Payments</a></li>
                <li class="divider"></li>
                <li><a tabindex="-1" href="Logout.php">Logout</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div> 
    

    <div class="sidebar-nav">
    <ul>
	  <!-- batas -->
	
    

   <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Dashboard<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">
            
   <li ><a href="index.php"><span class="fa fa-caret-right"></span> index</a></li>
    </ul></li>
  
  <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Inventaris<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">
            
       <li ><a href="data_inventaris.php"><span class="fa fa-caret-right"></span> Data Inventaris</a></li>
       <li ><a href="data_level.php"><span class="fa fa-caret-right"></span> Data Level</a></li>
      <li ><a href="inventaris.php"><span class="fa fa-caret-right"></span> Data barang masuk</a></li>
      <li ><a href="users.html"><span class="fa fa-caret-right"></span> Data barang keluar</a></li>
        


       <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Transaksi<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">


      <li ><a href="peminjaman.php"><span class="fa fa-caret-right"></span> peminjaman</a></li>
      <li ><a href="inventaris.php"><span class="fa fa-caret-right"></span> pengembalian</a></li>
       


         <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Users<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">
    
       <li ><a href="petugas.php"><span class="fa fa-caret-right"></span> Data petugas</a></li>
     <li ><a href="pegawai.php"><span class="fa fa-caret-right"></span> Data pegawai</a></li>
          
          <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Laporan<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">

      <li ><a href="ctk_inventaris1.php"><span class="fa fa-caret-right"></span>Data Laporan Absen</a></li>


       <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i> Pengaturan<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">


    <li ><a href="backup.php"><span class="fa fa-caret-right"></span> backup</a></li>


    </ul></li>
           
            </ul>
    </div>

    <div class="content">
        <div class="header">
            
            <h1 class="page-title">Data Barang Masuk</h1>
                    <ul class="breadcrumb">
            <li><a href="index.html">Home</a> </li>
            <li class="active">Users</li>
        </ul>

        </div>
        <div class="main-content">
            


<div class="row">
  <div class="col-md-12"> 
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
      <form action="simpan_b.php" method="post"  name="form1" id="form1">
        <div class="form-group">
        <label>Nama Inventaris</label>
        <input type="text" id="nama" name="nama" class="form-control">
        </div>
    
        <div class="form-group">
        <label>Kondisi</label>
        <input type="text" id="kondisi" name="kondisi" class="form-control">
        </div>

        <div class="form-group">
        <label>Spesifikasi</label>
        <input type="text" id="spesifikasi" name="spesifikasi" class="form-control">
        </div>

        <div class="form-group">
        <label>Keterangan</label>
        <input type="text" id="keterangan" name="keterangan" class="form-control">
        </div>
        
       
        <div class="form-group">
        <label>Jumlah</label>
        <input type="text" id="jumlah" name="jumlah" class="form-control">
        </div>

        <div class="form-group">
          <?php
        include "koneksi.php";
        $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
        $jsArray = "var id_jenis = new Array();\n";
        ?>
                  <label>ID Jenis</label>
                  <select class="form-control" name="id_jenis" onchange="changeValue(this.value)">
                    <option value="-12.0">..........Pilih ID Jenis..........
        <?php 
        while($row = mysqli_fetch_array($result)){
          echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
          $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
        ?></option>
        </select>           
          </div>

        <div class="form-group">
        <label>Tanggal</label>
        <input type="date" value="<?php $tgl=Date('Y-m-d'); echo $tgl;?>" id="tanggal_register" name="tanggal_register" class="form-control">
        </div>

        <div class="form-group">
          <?php
         


        include "koneksi.php";
        $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
        $jsArray = "var id_ruang = new Array();\n";
        ?>
                  <label>ID Ruang</label>
                  <select class="form-control" name="id_ruang" onchange="changeValue(this.value)">
                    <option value="-12.0">..........Pilih ID Ruang..........
        <?php 
        while($row = mysqli_fetch_array($result)){
          echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
          $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
        ?></option>
        </select>          
          </div>

        
         
          <div class="form-group">
           <?php
          $koneksi = mysqli_connect("localhost","root","","inventaris_smk");
          $cari_kd=mysqli_query($koneksi, "select max(kode_inventaris) as kode from inventaris");
          //besar atau kode yang baru masuk
          $tm_cari = mysqli_fetch_array($cari_kd);
          $kode =substr($tm_cari['kode'],1,4);
          $tambah =$kode+1;
          if ($tambah<10){
            $kode_inventaris="V000".$tambah;
          }else{
            $kode_inventaris="V00".$tambah;

          }
          ?>
        <label>Kode Inventaris</label>
        <input type="text" id="kode_inventaris" value="<?php echo $kode_inventaris;?>" name="kode_inventaris" class="form-control">
        </div>

        <div class="form-group">
        <?php
        include "koneksi.php";
        $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
        $jsArray = "var id_petugas = new Array();\n";
        ?>
                  <label>ID Petugas</label>
                  <select id="id_petugas" class="form-control" name="id_petugas" onchange="changeValue(this.value)">
                    <option value="-12.0">..........Pilih ID Petugas..........
                       <?php 
        while($row = mysqli_fetch_array($result)){
          echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
          $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
        ?>
      </option>
        </select>           
        </div>

          <div class="form-group">
        <label>Sumber</label>
        <input type="text" id="Sumber" name="sumber" class="form-control">
        </div>

      <div class="btn-toolbar list-toolbar">
      <button type="submit" name="simpan" id="simpan" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
      <a href="#myModal" data-toggle="modal" class="btn btn-danger">Delete</a>
      </div>
      </form>
  
  </div>
</div>
</div>
</div> 


            <footer> 

                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p class="pull-right">A <a href="http://www.portnine.com/bootstrap-themes" target="_blank">Free Bootstrap Theme</a> by <a href="http://www.portnine.com" target="_blank">Portnine</a></p>
                <p>@ujikom 2019 <a href="http://www.portnine.com" target="_blank">Portnine</a></p>
            </footer>
        </div>
    </div>


    
  
</body>
    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
</html>
